package ru.mf.sso.demoui.model.predictives

data class CheckTypeParameter(
    var id: Int,
    var name: String,
    var code: String,
    var required: Boolean,
    var type: Type
)
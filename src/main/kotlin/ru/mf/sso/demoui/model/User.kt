package ru.mf.sso.demoui.model

data class User(
    var theme: GuiThemeEnum?,
    var locale: String?,
    var name: String?
) {
}
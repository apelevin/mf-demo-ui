package ru.mf.sso.demoui.model.predictives

data class CheckInstanceParameter
    (
    var id: Int?,
    var checkTypeParameter: CheckTypeParameter,
    var value: String?
)
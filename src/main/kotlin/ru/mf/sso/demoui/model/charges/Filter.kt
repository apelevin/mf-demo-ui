package ru.mf.sso.demoui.model.charges

class Filter(
    var stringFilter: String?,
    var chargeTypeIds: MutableSet<Int>?,
    var name: String?,
    var idNotIn: MutableSet<Int>?
) {
    constructor() : this(null, null, null, null)
}
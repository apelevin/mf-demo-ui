package ru.mf.sso.demoui.model.predictives

data class Filter(
    var stringFilter: String?,
    var checkTypeIds: MutableSet<Int>?,
    var active: Boolean?,
    var name: String?,
    var code: String?,
    var idNotIn : MutableSet<Int>?
) {
    constructor() : this(null, null, null, null, null,null)
}
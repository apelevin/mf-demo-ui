package ru.mf.sso.demoui.model.predictives

class Type(
    var id: Int,
    var name: String
)
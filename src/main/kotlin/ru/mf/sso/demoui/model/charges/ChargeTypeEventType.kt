package ru.mf.sso.demoui.model.charges

data class ChargeTypeEventType
    (
    var eventTypeId: Int?,
    var chargeType: ChargeType?
) {
}
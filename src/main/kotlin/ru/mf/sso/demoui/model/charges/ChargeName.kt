package ru.mf.sso.demoui.model.charges

data class ChargeName(
    var id: Int?,
    var name: String?,
    var nameIvr: String?,
    var nameChat: String?
) {
}
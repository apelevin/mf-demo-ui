package ru.mf.sso.demoui.model.predictives


data class CheckInstance(
    var id: Int?,
    var checkType: CheckType?,
    var name: String?,
    var code: String?,
    var active: Boolean?,
    var parameters: List<CheckInstanceParameter>?
) {
    constructor() : this(null, null, null, null, null, emptyList())
}

package ru.mf.sso.demoui.model.charges

data class ChargeEventType(
    var id: Int?,
    var chargeType: ChargeType?,
    var externalId: Int?,
    var chargeName: ChargeName?
) {
    constructor() : this(
        null,
        null,
        null,
        ChargeName(null, null, null, null)
    )
}
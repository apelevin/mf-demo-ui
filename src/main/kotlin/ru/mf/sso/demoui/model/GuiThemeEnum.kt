package ru.mf.sso.demoui.model

enum class GuiThemeEnum {
    LIGHT, DARK
}

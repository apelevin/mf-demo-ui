package ru.mf.sso.demoui.model.charges

data class ChargeType(
    var id: Int?,
    var name: String?
) {
}
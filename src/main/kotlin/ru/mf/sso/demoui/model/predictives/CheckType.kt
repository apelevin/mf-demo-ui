package ru.mf.sso.demoui.model.predictives

data class CheckType(
    var id: Int,
    var name: String,
    var code: String,
    var segment: String,
    var active: Boolean,
    var parameters: MutableSet<CheckTypeParameter>
)

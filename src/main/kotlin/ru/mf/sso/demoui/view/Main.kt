package ru.mf.sso.demoui.view

import com.github.mvysny.karibudsl.v10.*
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.HasComponents
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.applayout.AppLayout
import com.vaadin.flow.component.applayout.DrawerToggle
import com.vaadin.flow.component.avatar.Avatar
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.contextmenu.MenuItem
import com.vaadin.flow.component.contextmenu.SubMenu
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.html.H3
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.menubar.MenuBar
import com.vaadin.flow.component.menubar.MenuBarVariant
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.page.AppShellConfigurator
import com.vaadin.flow.component.tabs.Tab
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.component.tabs.TabsVariant
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.router.*
import com.vaadin.flow.server.AppShellSettings
import com.vaadin.flow.theme.lumo.Lumo
import ru.mf.sso.demoui.model.GuiThemeEnum
import ru.mf.sso.demoui.model.User
import ru.mf.sso.demoui.service.UserService
import java.util.*


@Route("")
@RouteAlias("")
class Main(
    private val userService: UserService,
    private val supportedLocales: ru.mf.sso.demoui.config.SupportedLocales
) : AppLayout(), HasDynamicTitle, AppShellConfigurator, BeforeEnterObserver {
    private val menu: Tabs

    private val userProfileBinder = Binder(User::class.java).apply {
        bean = userService.getUser()
    }


    init {
        primarySection = Section.NAVBAR
        val appName = Span(
            H3(getTranslation("app.title"))
        )
        appName.addClassName("hide-on-mobile")

        val avatar = getAvatar()

        val layout = HorizontalLayout()
        layout.add(appName, avatar)
        layout.width = "100%"
        layout.height = "50px"
        layout.isPadding = true
        layout.isSpacing = true
        layout.isMargin = false
        layout.defaultVerticalComponentAlignment = FlexComponent.Alignment.CENTER
        layout.expand(appName)

        addToNavbar(true, DrawerToggle(), layout)
        menu = createMenuTabs()
        addToDrawer(menu)
    }


    private fun createMenuTabs(): Tabs {
        val tabs = Tabs()
        tabs.orientation = Tabs.Orientation.VERTICAL
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL)
        tabs.setId("tabs")
        tabs.add(*availableTabs)
        return tabs
    }

    private val availableTabs: Array<Tab>
        get() {
            val tabs: MutableList<Tab> = ArrayList()
            tabs.add(createTab(getTranslation("main.tabs.predictives"), Predictives::class.java))
            tabs.add(createTab(getTranslation("main.tabs.charges"), Charges::class.java))
            return tabs.toTypedArray()
        }

    companion object {

        private fun createTab(
            title: String,
            viewClass: Class<out Component>
        ): Tab {
            return createTab(populateLink(RouterLink(viewClass), title))
        }

        private fun createTab(content: Component): Tab {
            val tab = Tab()
            tab.add(content)
            return tab
        }

        private fun <T : HasComponents?> populateLink(a: T, title: String): T {
            a!!.add(title)
            return a
        }
    }

    private fun getAvatar(): MenuBar {
        val avatar = Avatar(userService.getUser().name)
        avatar.isTooltipEnabled = true
        avatar.colorIndex = 2

        val menuBar = MenuBar()
        menuBar.addThemeVariants(MenuBarVariant.LUMO_TERTIARY_INLINE)
        val menuItem: MenuItem = menuBar.addItem(avatar)
        val subMenu: SubMenu = menuItem.subMenu
        subMenu.addItem(getTranslation("main.profile")) { getProfileDialog().open() }
        subMenu.addItem(getTranslation("main.signOut")) {
            userService.signOut()
            userProfileBinder.bean = userService.getUser()
            userProfileBinder.refreshFields()
        }
        return menuBar
    }

    private fun getProfileDialog(): Dialog {
        val dialog = Dialog()
        val theme = ComboBox<GuiThemeEnum>(getTranslation("main.theme"))
        theme.setItems(GuiThemeEnum.entries)
        theme.setItemLabelGenerator { getTranslation("GuiThemeEnum.$it") }
        theme.bind(userProfileBinder).bind(User::theme)
        theme.setWidthFull()

        val locale = ComboBox<Locale>(getTranslation("main.language"))
        locale.setItems(supportedLocales.locales)
        locale.setItemLabelGenerator { it.displayLanguage }
        locale.bind(userProfileBinder).bind(
            { Locale.forLanguageTag(it.locale) }, { bean, value -> bean.locale = value.toLanguageTag() }
        )
        locale.setWidthFull()

        val save = Button(getTranslation("button.save"))
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
        save.onLeftClick {
            if (saveProfile()) {
                dialog.close()
                UI.getCurrent().page.reload()
            }
        }
        val cancel = Button(getTranslation("button.cancel"))
        cancel.onLeftClick {
            dialog.close()
        }
        val buttons = HorizontalLayout()
        buttons.add(save, cancel)
        buttons.setWidthFull()
        val verticalLayout = VerticalLayout()
        verticalLayout.add(theme, locale, buttons)
        dialog.add(verticalLayout)
        return dialog
    }

    private fun saveProfile(): Boolean {
        if (userProfileBinder.validate().isOk) {
            userService.saveUserPrefs(userProfileBinder.bean)
            return true
        }
        return false
    }


    override fun configurePage(settings: AppShellSettings) {
        //  settings.addFavIcon("icon", "icons/favicon.ico", "180x180")
    }

    override fun getPageTitle(): String {
        return getTranslation("page.title.main")
    }

    override fun beforeEnter(event: BeforeEnterEvent?) {
        val userTheme = userService.getUser().theme
        if (GuiThemeEnum.DARK == userTheme) {
            UI.getCurrent()?.element?.themeList?.add(Lumo.DARK)
        } else {
            UI.getCurrent()?.element?.themeList?.remove(Lumo.DARK)
        }
    }
}

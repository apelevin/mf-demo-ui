package ru.mf.sso.demoui.view

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.multiSelectComboBox
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.combobox.MultiSelectComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.grid.ColumnTextAlign
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.Grid.SelectionMode
import com.vaadin.flow.component.grid.GridSortOrder
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextArea
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.data.provider.SortDirection
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.theme.lumo.LumoIcon
import org.apache.commons.lang3.StringUtils
import ru.mf.sso.demoui.model.predictives.CheckInstance
import ru.mf.sso.demoui.model.predictives.CheckInstanceParameter
import ru.mf.sso.demoui.model.predictives.CheckType
import ru.mf.sso.demoui.model.predictives.Filter
import ru.mf.sso.demoui.service.CheckInstanceService
import ru.mf.sso.demoui.service.CheckTypeService
import ru.mf.sso.demoui.util.SortUtil
import java.time.OffsetDateTime
import java.util.*


private const val FILTER_ATTRIBUTE_IN_SESSION = "predictiveCheckSettingFilter"

@Route("/predictives", layout = Main::class)
@PageTitle("Predictives")
class Predictives(
    private val checkInstanceService: CheckInstanceService,
    private val checkTypeService: CheckTypeService
) : Div() {

    private var dialog: Dialog
    private lateinit var checkTypeSelectorField: ComboBox<CheckType>
    private lateinit var parameterGrid: Grid<CheckInstanceParameter>
    private var checkInstanceBinder = Binder(CheckInstance::class.java)
    private var checkInstanceParameterBinder: Binder<List<CheckInstanceParameter>> =
        Binder<List<CheckInstanceParameter>>()
    private var filterBinder = Binder(Filter::class.java).apply {
        bean = Filter()
        addValueChangeListener { filterChanged() }
    }

    private val checkInstanceDataProvider=DataProvider.fromCallbacks<CheckInstance>({ query ->
            val sort: MutableList<String> = ArrayList()
            if (query?.sortOrders?.isNotEmpty() == true) {
                sort.addAll(SortUtil.buildSortParam(query.sortOrders).toMutableList())
            } else {
                sort.add("id:desc")
            }
            checkInstanceService.getCheckInstances(
                filterBinder.bean,
                query?.limit ?: 1,
                query?.offset ?: 0,
                sort
            ).stream()
        }, { _ ->
            checkInstanceService.getCheckInstanceSize(filterBinder.bean)
        })

    init {
        dialog = dialog {
            width = "50em"
            verticalLayout {
                horizontalLayout {
                    checkTypeSelectorField = comboBox {
                        setItems(checkTypeService.getCheckTypes())
                        setItemLabelGenerator { it.name }
                        label = getTranslation("predictives.type")
                        width = "20em"
                        bind(checkInstanceBinder).asRequired()
                            .bind(CheckInstance::checkType)
                        isReadOnly = true
                        addValueChangeListener {
                            initDialogParameterTable(checkInstanceBinder.bean)
                        }
                    }
                    select<Boolean> {
                        label = getTranslation("predictives.active")
                        setItems(listOf(true, false))
                        setItemLabelGenerator { getTranslation(it) }
                        bind(checkInstanceBinder)
                            .asRequired()
                            .bind(CheckInstance::active)
                        width = "20em"
                    }
                }
                horizontalLayout {
                    textField(getTranslation("field.name")) {
                        width = "20em"
                        bind(checkInstanceBinder)
                            .asRequired()
                            .withValidator({ value -> !value.isNullOrBlank() }, "")
                            .withValidator({ value ->
                                checkInstanceService.getCheckInstanceSize(
                                    Filter(
                                        null,
                                        null,
                                        null,
                                        value,
                                        null,
                                        listOfNotNull(checkInstanceBinder.bean.id).toMutableSet()
                                    )
                                ) == 0
                            }, getTranslation("error.item.exists"))
                            .bind(CheckInstance::name)
                    }
                    textField(getTranslation("field.code")) {
                        width = "20em"
                        bind(checkInstanceBinder)
                            .asRequired()
                            .withValidator({ value -> !value.isNullOrBlank() }, "")
                            .withValidator({ value ->
                                checkInstanceService.getCheckInstanceSize(
                                    Filter(
                                        null,
                                        null,
                                        null,
                                        null,
                                        value,
                                        listOfNotNull(checkInstanceBinder.bean.id).toMutableSet()
                                    )
                                ) == 0
                            }, getTranslation("error.item.exists"))
                            .bind(CheckInstance::code)
                    }
                }

                nativeLabel { text = getTranslation("predictives.parameters") }
                parameterGrid = grid {
                    addThemeVariants(
                        GridVariant.LUMO_COMPACT,
                        GridVariant.LUMO_NO_BORDER,
                        GridVariant.LUMO_ROW_STRIPES
                    )
                    column({ it.checkTypeParameter.name })
                    {
                        setHeader(getTranslation("field.name"))
                        flexGrow = 1
                    }
                    componentColumn({ getParameterValueField(it) }) {
                        setHeader(getTranslation("field.value"))
                        flexGrow = 3
                    }
                    height = "20em"
                    if (checkInstanceBinder.bean != null) {
                        setItems(checkInstanceBinder.bean.parameters)
                    }
                }
                horizontalLayout {
                    justifyContentMode = FlexComponent.JustifyContentMode.END
                    setWidthFull()
                    button(getTranslation("button.save")) {
                        addThemeVariants(ButtonVariant.LUMO_PRIMARY)
                        onLeftClick {
                            save()
                        }
                        minWidth = "10em"
                    }
                    button(getTranslation("button.cancel")) {
                        onLeftClick {
                            closeDialog()
                        }
                        minWidth = "10em"
                    }

                }
            }
            addDialogCloseActionListener {
                closeDialog()
            }
        }
        setHeightFull()

                grid<CheckInstance> {

                    setHeightFull()
                    minWidth = "1000px"
                    addThemeVariants(
                        GridVariant.LUMO_NO_BORDER,
                        GridVariant.LUMO_ROW_STRIPES
                    )
                    dataProvider = checkInstanceDataProvider

                    val idCol = column(CheckInstance::id) {
                        setHeader(getTranslation("predictives.id"))
                        setSortProperty("id")
                        width = "5rem"
                        flexGrow = 0
                        textAlign = ColumnTextAlign.END

                    }

                    val codeCol = addColumn(getNameRenderer())
                        .setHeader(getTranslation("field.name"))
                        .setSortProperty("name")
                        .setFlexGrow(2)

                    val typeCol = column({ it.checkType?.name })
                    {
                        setHeader(getTranslation("predictives.type"))
                        setSortProperty("checkType.name")

                        flexGrow = 1
                    }
                    val activeCol = componentColumn({ getActiveBadge(it.active) })
                    {
                        setHeader(getTranslation("predictives.active"))
                        setSortProperty("active")
                        width = "10rem"
                        flexGrow = 0
                        textAlign = ColumnTextAlign.CENTER
                    }

                    componentColumn({ getParametersTable(it) })
                    {
                        setHeader(getTranslation("predictives.parameters"))
                        flexGrow = 4

                    }
                    val actionCol = componentColumn({ getRowButton(it) })
                    {
                        width = "70px"
                        flexGrow = 0
                    }

                    val hRow = prependHeaderRow()
                    hRow.join(idCol, codeCol).component = getTextFilterField()
                    hRow.getCell(typeCol).component = getCheckTypeFilter()
                    hRow.getCell(activeCol).component = getActiveFilter()
                    hRow.getCell(actionCol).component = getAddFilterButton()

                    sort(arrayListOf(GridSortOrder(idCol, SortDirection.DESCENDING)))
                }
                checkInstanceDataProvider.withConfigurableFilter()


        initFilter()
    }

    private fun getNameRenderer(): LitRenderer<CheckInstance> {
        return LitRenderer.of<CheckInstance>(
            "<div>\${item.name}<br>" +
                    "<small style=\"color: var(--lumo-secondary-text-color);\">\${item.code}</small><br>" +
                    "</div>"
        )
            .withProperty("name", CheckInstance::name)
            .withProperty("code", CheckInstance::code)
    }

    private fun getAddFilterButton(): HorizontalLayout {
        return horizontalLayout {
            setWidthFull()
            button {
                icon = VaadinIcon.PLUS.create()
                element.style["margin-left"]= "auto"
                addThemeVariants(
                    ButtonVariant.LUMO_SMALL,
                    ButtonVariant.LUMO_PRIMARY
                )
                onLeftClick {
                    openDialog(CheckInstance())
                }
            }
            removeFromParent()
        }
    }

    private fun getRowButton(checkInstance: CheckInstance): HorizontalLayout {
        return horizontalLayout {
            setWidthFull()
            button {
                icon = LumoIcon.EDIT.create()
                element.style["margin-left"]= "auto"
                addThemeVariants(
                    ButtonVariant.LUMO_SMALL
                )
                onLeftClick {
                    openDialog(checkInstance)
                }
            }
            removeFromParent()
        }
    }

    private fun openDialog(checkInstance: CheckInstance) {
        parameterGrid.setItems(emptyList())
        checkInstanceBinder.bean = checkInstance
        initDialogParameterTable(checkInstance)
        checkInstanceBinder.refreshFields()
        if (checkInstance.checkType == null) {
            checkTypeSelectorField.isReadOnly = false
        }
        dialog.open()
    }

    private fun closeDialog() {
        dialog.close()
        parameterGrid.setItems(emptyList())
        checkInstanceBinder.bean = CheckInstance()
        checkInstanceBinder.refreshFields()
        checkInstanceParameterBinder = Binder()

    }

    private fun initDialogParameterTable(checkInstance: CheckInstance) {
        checkInstanceParameterBinder = Binder<List<CheckInstanceParameter>>().apply {
            bean = checkInstance.parameters?.ifEmpty {
                checkTypeService.getCheckTypeParametersForCheckType(checkInstanceBinder.bean.checkType?.id)?.map {
                    CheckInstanceParameter(null, it, null)
                }?.toList()
            }
        }
        parameterGrid.setItems(checkInstanceParameterBinder.bean ?: emptyList())
    }

    private fun getCheckTypeFilter(): MultiSelectComboBox<CheckType> {
        return multiSelectComboBox {
            isClearButtonVisible = true
            setItemLabelGenerator { it.name }
            isClearButtonVisible = true
            setItems(checkTypeService.getCheckTypes())
            setItemLabelGenerator { it.name }
            bind(filterBinder).bind({ bean ->
                checkTypeService.getCheckTypes().filter { bean.checkTypeIds?.contains(it.id) ?: false }.toMutableSet()
            },
                { bean, value -> bean.checkTypeIds = value.map { it.id }.toMutableSet() })
            setWidthFull()
            removeFromParent()
        }
    }


    private fun getTextFilterField(): TextField {
        return textField {
            isClearButtonVisible = true
            setWidthFull()
            bind(filterBinder).bind(Filter::stringFilter) { bean, fieldValue ->
                bean.stringFilter = if (fieldValue.isNullOrEmpty())
                    null
                else
                    StringUtils.trim(fieldValue)
            }
            removeFromParent()
        }
    }

    private fun getActiveFilter(): ComboBox<Boolean> {
        return comboBox {
            setItems(listOf(true, false))
            isClearButtonVisible = true
            setItemLabelGenerator { if (it) "Yes" else "No" }
            bind(filterBinder)
                .bind("active")
            setWidthFull()
            removeFromParent()

        }
    }

    private fun getActiveBadge(activated: Boolean?): Span {
        return span {
            if (activated == true) {
                text = getTranslation("true")
                element.themeList.add("badge success")
            } else {
                text = getTranslation("false")
                element.themeList.add("badge error")
            }
            removeFromParent()
        }
    }

    private fun getParametersTable(checkInstance: CheckInstance): Grid<CheckInstanceParameter> {
        return grid {
            setSelectionMode(SelectionMode.NONE)
            column({ it.checkTypeParameter.name })
            {
                flexGrow = 1
            }
            column(CheckInstanceParameter::value)
            {
                flexGrow = 3
            }
            setHeightFull()
            isAllRowsVisible = true
            addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_COMPACT)
            if (checkInstance.parameters != null) {
                setItems(checkInstance.parameters)
            }
            removeFromParent()
        }
    }


    private fun getParameterValueField(checkInstanceParameter: CheckInstanceParameter): TextArea {
        return textArea {
            //  value = checkInstanceParameter.value ?: ""
            setWidthFull()
            maxLength = 1024
            isClearButtonVisible = true
            removeFromParent()
            bind(checkInstanceParameterBinder)
                .withValidator({ value -> !value.isNullOrBlank() }, "")
                .bind(
                    { bean ->
                        bean?.firstOrNull { it.checkTypeParameter.id == checkInstanceParameter.checkTypeParameter.id }?.value
                            ?: ""
                    },
                    { bean, value ->
                        bean?.first { it.checkTypeParameter.id == checkInstanceParameter.checkTypeParameter.id }?.value =
                            if (value.isNullOrBlank()) null else value.trim()
                    })
        }
    }

    private fun initFilter() {
        val filterLastUpdate: OffsetDateTime =
            Optional.ofNullable(UI.getCurrent().session.getAttribute(FILTER_ATTRIBUTE_IN_SESSION + "SaveDate"))
                .orElse(OffsetDateTime.now().minusHours(1)) as OffsetDateTime

        if (filterLastUpdate.plusHours(1) > OffsetDateTime.now()) {
            filterBinder.bean = UI.getCurrent().session.getAttribute(FILTER_ATTRIBUTE_IN_SESSION) as Filter
        }
    }

    private fun filterChanged() {
        checkInstanceDataProvider.refreshAll()
        saveFilter()
    }

    private fun saveFilter() {
        UI.getCurrent().session.setAttribute(FILTER_ATTRIBUTE_IN_SESSION + "SaveDate", OffsetDateTime.now())
        UI.getCurrent().session.setAttribute(FILTER_ATTRIBUTE_IN_SESSION, filterBinder.bean)
    }


    private fun save() {
        if (checkInstanceBinder.validate().isOk && checkInstanceParameterBinder.validate().isOk) {
            val checkInstance = checkInstanceBinder.bean
            checkInstance.parameters = checkInstanceParameterBinder.bean
            checkInstanceService.saveCheckInstance(checkInstance)
            checkInstanceBinder.bean = CheckInstance()
            checkInstanceParameterBinder = Binder<List<CheckInstanceParameter>>()
            parameterGrid.setItems(emptyList())
            checkInstanceDataProvider.refreshAll()
            Notification.show(getTranslation("notification.saved"))
            dialog.close()
        }
    }
}

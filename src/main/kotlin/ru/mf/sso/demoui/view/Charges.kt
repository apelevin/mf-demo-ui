package ru.mf.sso.demoui.view

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.karibudsl.v23.multiSelectComboBox
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.combobox.MultiSelectComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.grid.ColumnTextAlign
import com.vaadin.flow.component.grid.GridSortOrder
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.tabs.Tab
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.component.tabs.TabsVariant
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.upload.Upload
import com.vaadin.flow.component.upload.receivers.MemoryBuffer
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.data.provider.SortDirection
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.router.Route
import com.vaadin.flow.theme.lumo.LumoIcon
import org.apache.commons.lang3.StringUtils
import ru.mf.sso.demoui.model.charges.ChargeEventType
import ru.mf.sso.demoui.model.charges.ChargeType
import ru.mf.sso.demoui.model.charges.Filter
import ru.mf.sso.demoui.service.ChargeService
import ru.mf.sso.demoui.util.SortUtil
import java.time.OffsetDateTime
import java.util.*


private const val FILTER_ATTRIBUTE_IN_SESSION = "chargesSettingFilter"

@Route("/charges", layout = Main::class)
class Charges(
    private val chargeService: ChargeService
) : Div() {

    private var dialog: Dialog = Dialog()
    private lateinit var chargeTypeSelectorField: ComboBox<ChargeType>
    private lateinit var tabs: Tabs
    private var chargeEventTypeBinder = Binder(ChargeEventType::class.java).apply {
        bean = ChargeEventType()
        addValueChangeListener {
            tabs.isVisible = this.bean.id == null
        }
    }
    private var filterBinder = Binder(Filter::class.java).apply {
        bean = Filter()
        addValueChangeListener { filterChanged() }
    }

    private var chargeEventTypes: List<ChargeEventType>? = null
    private var buffer = MemoryBuffer()
    private lateinit var upload: Upload

    private val chargeEventTypeDataProvider = DataProvider.fromCallbacks<ChargeEventType>({ query ->
        val sort: MutableList<String> = ArrayList()
        if (query?.sortOrders?.isNotEmpty() == true) {
            sort.addAll(SortUtil.buildSortParam(query.sortOrders).toMutableList())
        } else {
            sort.add("id:desc")
        }
        chargeService.getChargeEventTypes(
            filterBinder.bean,
            query?.limit ?: 1,
            query?.offset ?: 0,
            sort
        ).stream()
    }, { _ ->
        chargeService.getChargeEventTypeSize(filterBinder.bean)
    })

    init {
        dialog = dialog {
            width = "30em"
            val singleTab = Tab(getTranslation("charges.create.singleForm"))
            val fileTab = Tab(getTranslation("charges.create.fromFile"))
            tabs = tabs {
                add(singleTab)
                add(fileTab)
                selectedIndex = 0
                addThemeVariants(TabsVariant.LUMO_CENTERED)
            }
            val singleContent = verticalLayout {
                justifyContentMode = FlexComponent.JustifyContentMode.CENTER
                textField(getTranslation("field.name")) {
                    width = "20em"
                    bind(chargeEventTypeBinder)
                        .asRequired()
                        .withValidator({ value -> !value.isNullOrBlank() }, "")
                        .withValidator({ value ->
                            chargeService.getChargeEventTypeSize(
                                Filter(
                                    null,
                                    null,
                                    value,
                                    listOfNotNull(chargeEventTypeBinder.bean.id).toMutableSet()
                                )
                            ) == 0
                        }, getTranslation("error.item.exists"))
                        .bind({ it.chargeName?.name }, { bean, value -> bean.chargeName?.name = value })
                }
                textField(getTranslation("charges.nameIvr")) {
                    width = "20em"
                    bind(chargeEventTypeBinder)
                        .asRequired()
                        .withValidator({ value -> !value.isNullOrBlank() }, "")
                        .bind({ it.chargeName?.nameIvr }, { bean, value -> bean.chargeName?.nameIvr = value })
                }
                textField(getTranslation("charges.nameChat")) {
                    width = "20em"
                    bind(chargeEventTypeBinder)
                        .asRequired()
                        .withValidator({ value -> !value.isNullOrBlank() }, "")
                        .bind({ it.chargeName?.nameChat }, { bean, value -> bean.chargeName?.nameChat = value })
                }
                chargeTypeSelectorField = comboBox {
                    setItems(chargeService.getChargeTypes())
                    setItemLabelGenerator { it.name }
                    label = getTranslation("charges.type")
                    width = "20em"
                    bind(chargeEventTypeBinder).asRequired()
                        .bind(ChargeEventType::chargeType)

                }
                integerField(getTranslation("charges.externalId")) {
                    width = "20em"
                    bind(chargeEventTypeBinder)
                        .asRequired()
                        .withValidator({ value -> value != null }, "")
                        .bind(ChargeEventType::externalId)
                }


            }

            val fileContent = verticalLayout {
                isVisible = false
                buffer = MemoryBuffer()
                upload = init(Upload(buffer)) {
                    setWidthFull()
                    maxFiles = 1
                    uploadButton = button(getTranslation("upload.uploadButton"))
                    dropLabel = nativeLabel(getTranslation("upload.dropLabel"))
                    addSucceededListener {
                        chargeEventTypes = chargeService.parseFromFile(buffer.inputStream)
                    }
                }
            }
            horizontalLayout {
                justifyContentMode = FlexComponent.JustifyContentMode.CENTER
                setWidthFull()
                button(getTranslation("button.save")) {
                    addThemeVariants(ButtonVariant.LUMO_PRIMARY)
                    onLeftClick {
                        save()
                    }
                    minWidth = "10em"
                }
                button(getTranslation("button.cancel")) {
                    onLeftClick {
                        closeDialog()
                    }
                    minWidth = "10em"
                }
            }

            tabs.addSelectedChangeListener {
                singleContent.isVisible = it.selectedTab == singleTab
                fileContent.isVisible = it.selectedTab == fileTab

            }

            addDialogCloseActionListener {
                closeDialog()
            }
        }
        setHeightFull()

        grid<ChargeEventType>
        {

            setHeightFull()
            minWidth = "1000px"
            addThemeVariants(
                GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_ROW_STRIPES
            )
            dataProvider = chargeEventTypeDataProvider

            val idCol = column(ChargeEventType::id) {
                setHeader(getTranslation("charges.id"))
                setSortProperty("id")
                width = "5rem"
                flexGrow = 0
                textAlign = ColumnTextAlign.END

            }

            val nameCol = addColumn(getNameRenderer())
                .setHeader(getTranslation("field.name"))
                .setSortProperty("chargeName.name")
                .setFlexGrow(2)

            val externalIdCol = column({ it.externalId })
            {
                setHeader(getTranslation("charges.externalId"))
                setSortProperty(getTranslation("charges.externalId"))
                width = "10rem"
                flexGrow = 0
                textAlign = ColumnTextAlign.CENTER
            }
            val typeCol = column({ it.chargeType?.name })
            {
                setHeader(getTranslation("charges.type"))
                setSortProperty("chargeType.name")

                flexGrow = 1
            }


            val actionCol = componentColumn({ getRowButton(it) })
            {
                width = "70px"
                flexGrow = 0
            }

            val hRow = prependHeaderRow()
            hRow.join(idCol, nameCol, externalIdCol).component = getTextFilterField()
            hRow.getCell(typeCol).component = getChargeTypeFilter()
            hRow.getCell(actionCol).component = getAddFilterButton()
            sort(arrayListOf(GridSortOrder(idCol, SortDirection.DESCENDING)))
        }
        chargeEventTypeDataProvider.withConfigurableFilter()


        initFilter()
    }

    private fun getNameRenderer(): LitRenderer<ChargeEventType> {
        return LitRenderer.of<ChargeEventType>(
            "<div>\${item.name}<br>" +
                    "<small style=\"color: var(--lumo-secondary-text-color);\">\${item.nameIvr}</small><br>" +
                    "<small style=\"color: var(--lumo-secondary-text-color);\">\${item.nameChat}</small><br>" +
                    "</div>"
        )
            .withProperty("name") { it.chargeName?.name }
            .withProperty("nameIvr") { it.chargeName?.nameIvr }
            .withProperty("nameChat") { it.chargeName?.nameChat }
    }

    private fun getAddFilterButton(): HorizontalLayout {
        return horizontalLayout {
            setWidthFull()
            button {
                icon = VaadinIcon.PLUS.create()
                element.style["margin-left"] = "auto"
                addThemeVariants(
                    ButtonVariant.LUMO_SMALL,
                    ButtonVariant.LUMO_PRIMARY
                )
                onLeftClick {
                    openDialog(ChargeEventType())
                }
            }
            removeFromParent()
        }
    }

    private fun getRowButton(chargeEventType: ChargeEventType): HorizontalLayout {
        return horizontalLayout {
            setWidthFull()
            button {
                icon = LumoIcon.EDIT.create()
                element.style["margin-left"] = "auto"
                addThemeVariants(
                    ButtonVariant.LUMO_SMALL
                )
                onLeftClick {
                    openDialog(chargeEventType)
                }
            }
            removeFromParent()
        }
    }

    private fun openDialog(chargeEventType: ChargeEventType) {
        chargeEventTypeBinder.bean = chargeEventType
        chargeEventTypeBinder.refreshFields()
        dialog.open()
    }

    private fun closeDialog() {
        dialog.close()
        chargeEventTypeBinder.bean = ChargeEventType()
        chargeEventTypeBinder.refreshFields()
        chargeEventTypes = null
        upload.clearFileList()

    }

    private fun getChargeTypeFilter(): MultiSelectComboBox<ChargeType> {
        return multiSelectComboBox {
            isClearButtonVisible = true
            setItemLabelGenerator { it.name }
            isClearButtonVisible = true
            setItems(chargeService.getChargeTypes())
            setItemLabelGenerator { it.name }
            bind(filterBinder).bind({ bean ->
                chargeService.getChargeTypes().filter { bean.chargeTypeIds?.contains(it.id) ?: false }.toMutableSet()
            },
                { bean, value -> bean.chargeTypeIds = value.map { it.id!! }.toMutableSet() })
            setWidthFull()
            removeFromParent()
        }
    }


    private fun getTextFilterField(): TextField {
        return textField {
            isClearButtonVisible = true
            setWidthFull()
            bind(filterBinder).bind(Filter::stringFilter) { bean, fieldValue ->
                bean.stringFilter = if (fieldValue.isNullOrEmpty())
                    null
                else
                    StringUtils.trim(fieldValue)
            }
            removeFromParent()
        }
    }

    private fun initFilter() {
        val filterLastUpdate: OffsetDateTime =
            Optional.ofNullable(UI.getCurrent().session.getAttribute(FILTER_ATTRIBUTE_IN_SESSION + "SaveDate"))
                .orElse(OffsetDateTime.now().minusHours(1)) as OffsetDateTime

        if (filterLastUpdate.plusHours(1) > OffsetDateTime.now()) {
            filterBinder.bean = UI.getCurrent().session.getAttribute(FILTER_ATTRIBUTE_IN_SESSION) as Filter
        }
    }

    private fun filterChanged() {
        chargeEventTypeDataProvider.refreshAll()
        saveFilter()
    }

    private fun saveFilter() {
        UI.getCurrent().session.setAttribute(FILTER_ATTRIBUTE_IN_SESSION + "SaveDate", OffsetDateTime.now())
        UI.getCurrent().session.setAttribute(FILTER_ATTRIBUTE_IN_SESSION, filterBinder.bean)
    }


    private fun save() {
        if (chargeEventTypes == null || chargeEventTypes!!.isEmpty())
            if (chargeEventTypeBinder.validate().isOk) {
                val chargeEventType = chargeEventTypeBinder.bean
                chargeService.saveChargeEventType(listOf(chargeEventType))
                chargeEventTypeBinder.bean = ChargeEventType()

            } else {
                chargeService.saveChargeEventType(chargeEventTypes!!)
                chargeEventTypes = null
                upload.clearFileList()

            }
        chargeEventTypeDataProvider.refreshAll()
        Notification.show(getTranslation("notification.saved"))
        dialog.close()
    }
}
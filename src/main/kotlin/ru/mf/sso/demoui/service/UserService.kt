package ru.mf.sso.demoui.service

import ru.mf.sso.demoui.model.User

interface UserService {

    fun getUser(): User
    fun saveUserPrefs(user: User)
    fun signOut()
}
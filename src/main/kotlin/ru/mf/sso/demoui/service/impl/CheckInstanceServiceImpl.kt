package ru.mf.sso.demoui.service.impl

import com.github.mvysny.vokdataloader.SortClause
import com.github.mvysny.vokdataloader.sortedBy
import jakarta.enterprise.context.ApplicationScoped
import ru.mf.sso.demoui.model.predictives.CheckInstance
import ru.mf.sso.demoui.model.predictives.CheckInstanceParameter
import ru.mf.sso.demoui.model.predictives.CheckTypeParameter
import ru.mf.sso.demoui.model.predictives.Filter
import ru.mf.sso.demoui.service.CheckInstanceService
import ru.mf.sso.demoui.service.TestDataService



@ApplicationScoped
class CheckInstanceServiceImpl
    (testDataService: TestDataService) : CheckInstanceService {
    private val testData = testDataService.checkTestData
    private val testChecks = testDataService.checkTypes


    override fun getCheckInstances(filter: Filter?, limit: Int, offset: Int, sort: List<String>): List<CheckInstance> {

        val items = getFilteredTestData(filter)
        val start = if (offset >= items.size - 1) items.size - 1 else offset
        val end = if (start + limit > items.size) items.size else start + limit
        return items.toList().subList(start, end).onEach { it.parameters = getParametersList(it) }
            .sortedBy(getSortClauses(sort)[0])
    }

    override fun getCheckInstanceSize(filter: Filter?): Int {
        return getFilteredTestData(filter).size
    }

    override fun saveCheckInstance(checkInstance: CheckInstance) {
        checkInstance.id = checkInstance.id ?: (testData.keys.max() + 1)
        testData[checkInstance.id!!] = checkInstance
    }


    private fun getFilteredTestData(filter: Filter?): List<CheckInstance> {
        if (filter == null) {
            return testData.values.toList()
        }

        return testData.values.asSequence().filter { filter.active == null || it.active == filter.active }
            .filter { filter.checkTypeIds.isNullOrEmpty() || filter.checkTypeIds!!.contains(it.checkType?.id) }
            .filter {
                filter.stringFilter == null ||
                        it.name?.contains(filter.stringFilter!!) ?: false ||
                        it.code?.contains(filter.stringFilter!!) ?: false ||
                        it.id.toString().startsWith(filter.stringFilter!!)

            }
            .filter { filter.name.isNullOrBlank() || it.name.equals(filter.name) }
            .filter { filter.code.isNullOrBlank() || it.code.equals(filter.code) }
            .filter { filter.idNotIn.isNullOrEmpty()|| !filter.idNotIn!!.contains(it.id) }
            .toList()
    }

    private fun getSortClauses(sort: List<String>): List<SortClause> {
        return sort.map {
            val sortList = it.split(":")
            SortClause(sortList[0], sortList[1] == "asc")
        }.toList()
    }

    private fun getParametersList(checkInstance: CheckInstance?): List<CheckInstanceParameter> {
        if (checkInstance == null) {
            return emptyList()
        }
        val values =
            checkInstance.parameters?.associate { it.checkTypeParameter.id to it } ?: emptyMap()
        val parameters = getCheckTypeParametersForCheckType(checkInstance.checkType?.id)
        val out = parameters.map {
            CheckInstanceParameter(
                values[it.id]?.id,
                it,
                values[it.id]?.value
            )
        }.toList()
        return out
    }


    private fun getCheckTypeParametersForCheckType(checkTypeId: Int?): List<CheckTypeParameter> {
        if (checkTypeId == null) {
            return emptyList()
        }
        return testChecks[checkTypeId]?.parameters?.toList() ?: emptyList()
    }
}
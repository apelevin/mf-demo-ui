package ru.mf.sso.demoui.service

import jakarta.enterprise.context.ApplicationScoped
import ru.mf.sso.demoui.model.charges.ChargeEventType
import ru.mf.sso.demoui.model.charges.ChargeName
import ru.mf.sso.demoui.model.charges.ChargeType
import ru.mf.sso.demoui.model.predictives.*
import kotlin.random.Random

@ApplicationScoped
class TestDataService {

    private val CHARGE_EVENT_TYPE_SIZE = 20

    private val types = mapOf(
        "string" to Type(1, "String"), "number" to Type(2, "Number")
    )

    private val checkTypeParameters = mapOf(
        1 to CheckTypeParameter(1, "Parameter 1", "parameter1", true, types["string"]!!),
        2 to CheckTypeParameter(2, "Parameter 2", "parameter2", true, types["number"]!!),
        3 to CheckTypeParameter(3, "Parameter 3", "parameter3", true, types["string"]!!),
        4 to CheckTypeParameter(4, "Parameter 4", "parameter4", true, types["number"]!!)
    )

    val checkTypes = mapOf(
        1 to CheckType(
            1, "Check type 1", "CHECK_TYPE_1", "segment 1", true,
            setOf(checkTypeParameters[1]!!, checkTypeParameters[2]!!).toMutableSet()
        ),
        2 to CheckType(
            2, "Check type 2", "CHECK_TYPE_2", "segment 1", true,
            setOf(checkTypeParameters[3]!!).toMutableSet()
        ),
        3 to CheckType(
            3, "Check type 3", "CHECK_TYPE_3", "segment 2", true,
            setOf(checkTypeParameters[4]!!).toMutableSet()
        )
    )

    var checkTestData =
        mutableMapOf(
            1 to CheckInstance(
                1, checkTypes[1]!!, "test name 1", "test_code_1", true,
                listOf(
                    CheckInstanceParameter(1, checkTypeParameters[1]!!, "testValue 1"),
                    CheckInstanceParameter(2, checkTypeParameters[2]!!, "1")
                )
            ),
            2 to CheckInstance(
                2, checkTypes[1]!!, "test name 2", "test_code_2", true, listOf(
                    CheckInstanceParameter(3, checkTypeParameters[1]!!, "testValue 2"),
                    CheckInstanceParameter(4, checkTypeParameters[2]!!, "2")
                )
            ),
            3 to CheckInstance(
                3, checkTypes[1]!!, "test name 3", "test_code_3", true, listOf(
                    CheckInstanceParameter(5, checkTypeParameters[1]!!, "testValue 3"),
                    CheckInstanceParameter(6, checkTypeParameters[2]!!, "3")
                )
            ),
            4 to CheckInstance(
                4, checkTypes[1]!!, "test name 4", "test_code_4", true, listOf(
                    CheckInstanceParameter(
                        7,
                        checkTypeParameters[1]!!,
                        "503555;503556;503557;503558;503490;501682;501683;501684;501685;502722;502723;502724;502728;502732;502856;502976;502978;502979;502980;502981;502982;502790;502791;324003;324004;324005;324006;501631;501630;501628;501635;499353;499352;499489;499490;499555;499557;499554;499556;322800;322801;322802;322803;322804;322805;322806;322807;322808;503490;503809"
                    ),
                    CheckInstanceParameter(8, checkTypeParameters[2]!!, "4")
                )
            ),
            5 to CheckInstance(5, checkTypes[2]!!, "test name 5", "test_code_5", true, null),
            6 to CheckInstance(6, checkTypes[2]!!, "test name 6", "test_code_6", true, null),
            7 to CheckInstance(7, checkTypes[2]!!, "test name 7", "test_code_7", true, null),
            8 to CheckInstance(8, checkTypes[2]!!, "test name 8", "test_code_8", false, null),
            9 to CheckInstance(9, checkTypes[2]!!, "test name 9", "test_code_9", false, null),
            10 to CheckInstance(10, checkTypes[3]!!, "test name 10", "test_code_10", true, null),
            11 to CheckInstance(11, checkTypes[3]!!, "test name 11", "test_code_11", true, null)

        )


    val chargeTypes = mapOf(
        1 to ChargeType(1, "Charge type 1"),
        2 to ChargeType(2, "Charge type 2"),
        3 to ChargeType(3, "Charge type 3"),
        4 to ChargeType(4, "Charge type 4"),
        5 to ChargeType(5, "Charge type 5")
    );

    var chargeEventTypes = generateChargeEventTypes();


    private fun generateChargeEventTypes(): MutableMap<Int, ChargeEventType> {
        var out = mutableMapOf<Int, ChargeEventType>()
        for (i in 1..CHARGE_EVENT_TYPE_SIZE) {
            val charge = ChargeEventType(
                i,
                chargeTypes[Random.nextInt(1, 5)],
                i * 100, ChargeName(
                    i, "Charge name $i",
                    "Charge name for ivr $i",
                    "Charge name for chat $i"
                )
            )
            out[i] = charge;
        }
        return out
    }

}
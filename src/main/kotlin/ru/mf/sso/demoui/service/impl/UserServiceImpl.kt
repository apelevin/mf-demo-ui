package ru.mf.sso.demoui.service.impl

import com.vaadin.quarkus.annotation.VaadinSessionScoped
import io.quarkus.arc.Unremovable
import ru.mf.sso.demoui.model.GuiThemeEnum
import ru.mf.sso.demoui.model.User
import ru.mf.sso.demoui.service.UserService
import java.util.*

@VaadinSessionScoped
@Unremovable
class UserServiceImpl : UserService {
    private var user: User = getDefaultUser()
    override fun getUser(): User {
        return user
    }

    override fun saveUserPrefs(user: User) {
        this.user.theme = user.theme
        this.user.locale = user.locale
    }

    override fun signOut() {
        user = getDefaultUser()
    }

    private fun getDefaultUser(): User {
        return User(GuiThemeEnum.LIGHT, Locale.of("en", "US").toLanguageTag(), "Anonymous")
    }
}
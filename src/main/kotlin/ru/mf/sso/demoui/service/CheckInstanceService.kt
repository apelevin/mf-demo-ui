package ru.mf.sso.demoui.service

import ru.mf.sso.demoui.model.predictives.CheckInstance
import ru.mf.sso.demoui.model.predictives.Filter

interface CheckInstanceService {
    fun getCheckInstances(filter: Filter?, limit: Int, offset: Int, sort: List<String>): List<CheckInstance>
    fun getCheckInstanceSize(filter: Filter?): Int
    fun saveCheckInstance(checkInstance: CheckInstance)

}
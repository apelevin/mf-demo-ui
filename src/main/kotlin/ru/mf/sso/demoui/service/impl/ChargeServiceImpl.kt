package ru.mf.sso.demoui.service.impl

import com.github.mvysny.vokdataloader.SortClause
import com.github.mvysny.vokdataloader.sortedBy
import jakarta.enterprise.context.ApplicationScoped
import ru.mf.sso.demoui.model.charges.ChargeEventType
import ru.mf.sso.demoui.model.charges.ChargeType
import ru.mf.sso.demoui.model.charges.Filter
import ru.mf.sso.demoui.service.ChargeService
import ru.mf.sso.demoui.service.TestDataService
import java.io.InputStream
import java.util.*

@ApplicationScoped
class ChargeServiceImpl(private var testDataService: TestDataService) : ChargeService {
    private val testData = testDataService.chargeEventTypes
    override fun getChargeEventTypes(
        filter: Filter?,
        limit: Int,
        offset: Int,
        sort: List<String>
    ): List<ChargeEventType> {
        val items = getFilteredTestData(filter)
        val start = if (offset >= items.size - 1) items.size - 1 else offset
        val end = if (start + limit > items.size) items.size else start + limit
        return items.toList().subList(start, end)
            .sortedBy(getSortClauses(sort)[0])
    }

    override fun getChargeEventTypeSize(filter: Filter?): Int {
        return getFilteredTestData(filter).size
    }

    override fun saveChargeEventType(chargeEventTypes: List<ChargeEventType>) {
        chargeEventTypes.forEach {
            it.id = it.id ?: (testData.keys.max() + 1)
            testData[it.id!!] = it
        }
    }

    override fun parseFromFile(inputStream: InputStream): List<ChargeEventType> {

        return Collections.emptyList()
    }

    override fun getChargeTypes(): List<ChargeType> {
        return testDataService.chargeTypes.values.toList()
    }

    private fun getFilteredTestData(filter: Filter?): List<ChargeEventType> {
        if (filter == null) {
            return testData.values.toList()
        }

        return testData.values.asSequence()
            .filter { filter.chargeTypeIds.isNullOrEmpty() || filter.chargeTypeIds!!.contains(it.chargeType?.id) }
            .filter {
                filter.stringFilter == null ||
                        it.chargeName?.name?.contains(filter.stringFilter!!) ?: false ||
                        it.chargeName?.nameIvr?.contains(filter.stringFilter!!) ?: false ||
                        it.chargeName?.nameChat?.contains(filter.stringFilter!!) ?: false

            }
            .filter { filter.name.isNullOrBlank() || it.chargeName?.name.equals(filter.name) }
            .filter { filter.idNotIn.isNullOrEmpty() || !filter.idNotIn!!.contains(it.id) }
            .toList()
    }

    private fun getSortClauses(sort: List<String>): List<SortClause> {
        return sort.map {
            val sortList = it.split(":")
            SortClause(sortList[0], sortList[1] == "asc")
        }.toList()
    }
}
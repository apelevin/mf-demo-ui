package ru.mf.sso.demoui.service

import ru.mf.sso.demoui.model.charges.ChargeEventType
import ru.mf.sso.demoui.model.charges.ChargeType
import ru.mf.sso.demoui.model.charges.Filter
import java.io.InputStream


interface ChargeService {

    fun getChargeEventTypes(filter: Filter?, limit: Int, offset: Int, sort: List<String>): List<ChargeEventType>
    fun getChargeEventTypeSize(filter: Filter?): Int
    fun saveChargeEventType(chargeEventTypes: List<ChargeEventType>)
    fun getChargeTypes(): List<ChargeType>

    fun parseFromFile(inputStream: InputStream): List<ChargeEventType>
}
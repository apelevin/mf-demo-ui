package ru.mf.sso.demoui.service.impl

import jakarta.enterprise.context.ApplicationScoped
import ru.mf.sso.demoui.model.predictives.CheckType
import ru.mf.sso.demoui.model.predictives.CheckTypeParameter
import ru.mf.sso.demoui.service.CheckTypeService
import ru.mf.sso.demoui.service.TestDataService

@ApplicationScoped
class CheckTypeServiceImpl(testDataService: TestDataService) : CheckTypeService {

    private val checkTypes = testDataService.checkTypes

    override fun getCheckTypeParametersForCheckType(checkTypeId: Int?): Set<CheckTypeParameter>? {
        return checkTypes[checkTypeId]?.parameters
    }

    override fun getCheckTypes():List<CheckType>
    {
        return checkTypes.values.toList()
    }

}
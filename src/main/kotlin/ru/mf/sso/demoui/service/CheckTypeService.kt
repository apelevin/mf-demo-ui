package ru.mf.sso.demoui.service

import ru.mf.sso.demoui.model.predictives.CheckType
import ru.mf.sso.demoui.model.predictives.CheckTypeParameter

interface CheckTypeService {
    fun getCheckTypeParametersForCheckType(checkTypeId: Int?): Set<CheckTypeParameter>?
    fun getCheckTypes(): List<CheckType>
}
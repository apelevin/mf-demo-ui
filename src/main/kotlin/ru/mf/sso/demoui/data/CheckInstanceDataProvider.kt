package ru.mf.sso.demoui.data

import com.vaadin.flow.data.provider.AbstractDataProvider
import com.vaadin.flow.data.provider.Query
import jakarta.enterprise.context.ApplicationScoped
import ru.mf.sso.demoui.model.predictives.CheckInstance
import ru.mf.sso.demoui.model.predictives.Filter
import ru.mf.sso.demoui.service.impl.CheckInstanceServiceImpl
import ru.mf.sso.demoui.util.SortUtil
import java.util.stream.Stream

@ApplicationScoped
class CheckInstanceDataProvider(private val checkInstanceService: CheckInstanceServiceImpl) :
    AbstractDataProvider<CheckInstance, Filter>() {

    override fun fetch(query: Query<CheckInstance, Filter>?): Stream<CheckInstance> {
        val sort: MutableList<String> = ArrayList()
        if (query?.sortOrders?.isNotEmpty() == true) {
            sort.addAll(SortUtil.buildSortParam(query.sortOrders).toMutableList())
        } else {
            sort.add("id:desc")
        }
        return checkInstanceService.getCheckInstances(
            query?.filter?.orElse(null),
            query?.limit ?: 1,
            query?.offset ?: 0,
            sort
        ).stream()
    }

    override fun size(query: Query<CheckInstance, Filter>?): Int {
        return checkInstanceService.getCheckInstanceSize(query?.filter?.orElse(null))
    }

    override fun isInMemory(): Boolean {
        return false
    }


}
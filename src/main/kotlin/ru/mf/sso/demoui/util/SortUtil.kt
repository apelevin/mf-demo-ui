package ru.mf.sso.demoui.util

import com.vaadin.flow.data.provider.QuerySortOrder
import com.vaadin.flow.data.provider.SortDirection

object SortUtil {
    fun buildSortParam(querySortOrders: MutableList<QuerySortOrder>): List<String> {
        val sortList: MutableList<String> = ArrayList()
        for (queryOrder in querySortOrders) {
            sortList.add(queryOrder.sorted + ":" + if (queryOrder.direction == SortDirection.DESCENDING) "desc" else "asc")
        }
        return sortList
    }
}
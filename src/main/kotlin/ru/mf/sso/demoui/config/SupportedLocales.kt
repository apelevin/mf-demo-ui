package ru.mf.sso.demoui.config

import jakarta.enterprise.context.ApplicationScoped
import java.util.*

@ApplicationScoped
class SupportedLocales {
    val locales = listOf(
        Locale.of("en", "US"),
        Locale.of("ru", "RU")
    )
}
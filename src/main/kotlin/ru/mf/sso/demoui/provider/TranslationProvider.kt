package ru.mf.sso.demoui.provider

import com.vaadin.flow.i18n.I18NProvider
import com.vaadin.quarkus.annotation.VaadinServiceEnabled
import com.vaadin.quarkus.annotation.VaadinServiceScoped
import io.quarkus.arc.Unremovable
import org.slf4j.LoggerFactory
import ru.mf.sso.demoui.service.UserService
import java.text.MessageFormat
import java.util.*


@VaadinServiceEnabled
@VaadinServiceScoped
@Unremovable
class TranslationProvider(
    private val supportedLocales: ru.mf.sso.demoui.config.SupportedLocales,
    private val userService: UserService
) : I18NProvider {


    override fun getProvidedLocales(): List<Locale> {
        return supportedLocales.locales
    }

    override fun getTranslation(key: String?, locale: Locale, vararg params: Any?): String {
        if (key == null) {
            LoggerFactory.getLogger(TranslationProvider::class.java.name)
                .warn("Got lang request for key with null value!")
            return ""
        }
        val userLocale = Locale.forLanguageTag(userService.getUser().locale)
        val bundle = ResourceBundle.getBundle(BUNDLE_PREFIX, userLocale)
        var value: String
        value = try {
            bundle.getString(key)
        } catch (e: MissingResourceException) {
            LoggerFactory.getLogger(TranslationProvider::class.java.name)
                .warn("Missing resource", e)
            return "!" + userLocale.language + ": " + key
        }
        if (params.size > 0) {
            value = MessageFormat.format(value, *params)
        }
        return value
    }

    companion object {
        const val BUNDLE_PREFIX = "translation"
    }
}

